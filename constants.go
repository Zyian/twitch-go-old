package twitchgo

import "net/url"

const (
	//APIV5 - Twitch API Version 5 Header: Most Current as of 18 Apr 2017
	APIV5 = "application/vnd.twitchtv.v5+json"

	//APIV3 - Twitch API Version 3 Header: Deprecated API will soon be removed
	APIV3 = "application/vnd.twitchtv.v3+json"
)

var (
	//DefaultURL is the default pathway for Twitch API calls
	DefaultURL = &url.URL{
		Scheme: "https",
		Host:   "api.twitch.tv",
		Path:   "kraken",
	}
)
