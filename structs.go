package twitchgo

import (
	"net/http"
	"net/url"
)

type Session struct {
	Client     *http.Client
	URL        *url.URL
	ClientID   string
	APIVersion string
}

type NetError struct {
	Error      string `json:"error"`
	StatusCode int    `json:"status"`
	Message    string `json:"message"`
	Empty      bool
}

type UserGroup struct {
	Total int    `json:"_total"`
	Users []User `json:"users"`
}

type User struct {
	ID          string `json:"_id"`
	Bio         string `json:"bio"`
	DateCreated string `json:"created_at"`
	DisplayName string `json:"display_name"`
	Logo        string `json:"logo"`
	Name        string `json:"name"`
	Type        string `json:"type"`
	DateUpdated string `json:"updated_at"`
}

type Stream struct {
	ID          int        `json:"_id"`
	Game        string     `json:"game"`
	Viewers     int        `json:"viewers"`
	VideoHeight int        `json:"video_height"`
	AverageFPS  float64    `json:"average_fps"`
	Delay       int        `json:"delay"`
	DateCreated string     `json:"created_at"`
	IsPlaylist  bool       `json:"is_playlist"`
	Preview     PreviewSet `json:"preview"`
	Channel     Channel    `json:"channel"`
}

type PreviewSet struct {
	Small    string `json:"small"`
	Medium   string `json:"medium"`
	Large    string `json:"large"`
	Template string `json:"template"`
}

type Channel struct {
	ID                    int    `json:"_id"`
	BroadcasterLanguage   string `json:"broadcaster_language"`
	DateCreated           string `json:"created_at"`
	DisplayName           string `json:"display_name"`
	Email                 string `json:"email"`
	Followers             int    `json:"followers"`
	Game                  string `json:"game"`
	Language              string `json:"language"`
	Logo                  string `json:"logo"`
	Mature                bool   `json:"mature"`
	Name                  string `json:"name"`
	Partner               bool   `json:"partner"`
	ProfileBanner         string `json:"profile_banner"`
	ProfileBannerBkgColor string `json:"profile_banner_background_color"`
	Status                string `json:"status"`
	DateUpdated           string `json:"updated_at"`
	URL                   string `json:"url"`
	VideoBanner           string `json:"video_banner"`
	Views                 int    `json:"views"`
}

type FollowGroup struct {
	Total   int      `json:"_total"`
	Follows []Follow `json:"follows"`
}

type Follow struct {
	DateCreated   string  `json:"created_at"`
	Notifications bool    `json:"notifications"`
	Channel       Channel `json:"channel"`
}
