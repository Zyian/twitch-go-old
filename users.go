package twitchgo

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strings"
)

//GetUserIDMap takes a []string of user names then returns a
// map with the key being the display name, and the value being the user ID
// ie. []string{"dansgaming", "twitch", "lirik"}
// ie Return: map["dansgaming"] == 7236692
func (session *Session) GetUserIDMap(userList []string) (map[string]string, error) {
	var userSet UserGroup
	var err error

	twitchUserID := make(map[string]string)

	userQueryList := "?login=" + strings.Join(userList, ",")

	userResp, err := session.sendReq("GET", "/users", userQueryList)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	readBody, err := ioutil.ReadAll(userResp.Body)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	jsonErr := json.Unmarshal(readBody, &userSet)
	if jsonErr != nil {
		fmt.Println("Error in JSON: ", jsonErr)
		return nil, jsonErr
	}

	//Error checking
	if userSet.Total < 1 {
		return nil, err
	}

	userResp.Body.Close()

	for _, val := range userSet.Users {
		twitchUserID[val.Name] = val.ID
	}

	return twitchUserID, nil
}

//GetUserByID - Takes a user ID and returns a struct containing all
//of the information of said user
func (session *Session) GetUserByID(userID string) *User {
	var reqUser *User
	apiResponse, err := session.sendReq("GET", "/users/", userID)
	if err != nil {
		fmt.Println("Error in GET User: ", err)
	}

	readBody, err := ioutil.ReadAll(apiResponse.Body)
	if err != nil {
		fmt.Println("Error reading Body: ", err)
	}

	jsonErr := json.Unmarshal([]byte(readBody), &reqUser)
	if jsonErr != nil {
		fmt.Println("Error in JSON: ", jsonErr)
	}
	apiResponse.Body.Close()

	return reqUser
}

//GetUserFollows takes a user ID and optional parameters and returns a struct containing the list
//of the channels that the user follows and the information pertaining to the followed channels
func (session *Session) GetUserFollows(userID string, args interface{}) *FollowGroup {
	var followSet *FollowGroup

	//TODO: Allows for arguments of the optional queries
	apiResponse, err := session.sendReq("GET", "/users/", userID+"/follows/channels")
	if err != nil {
		fmt.Println("Error in GET Follows: ", err)
	}

	readBody, err := ioutil.ReadAll(apiResponse.Body)
	if err != nil {
		fmt.Println("Error reading Body: ", err)
	}

	jsonErr := json.Unmarshal([]byte(readBody), &followSet)
	if jsonErr != nil {
		fmt.Println("Error in JSON: ", jsonErr)
	}
	apiResponse.Body.Close()

	return followSet
}

func CheckUserFollowsByChannel() {}
