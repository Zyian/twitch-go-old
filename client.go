package twitchgo

import (
	"fmt"
	"log"
	"net/http"
)

//NewClient takes a Client ID from Twitch API Dashboard and returns a new session
//to access other API calls.
func NewClient(clientID string) *Session {
	if clientID == "" {
		log.Fatal("No ClientID provided")
		return nil
	}

	return &Session{
		Client:     &http.Client{},
		URL:        DefaultURL,
		ClientID:   clientID,
		APIVersion: APIV5,
	}
}

func (s *Session) sendReq(method string, url string, qry string) (*http.Response, error) {
	req, err := http.NewRequest(method, s.URL.String()+url+qry, nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("ACCEPT", s.APIVersion)
	req.Header.Add("Client-ID", s.ClientID)

	resp, err := s.Client.Do(req)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}

	return resp, nil
}
