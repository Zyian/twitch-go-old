# RETIRED PROJECT

# Twitchgo
Golang Package for Twitch.tv API v5

Work is based on knspriggs's [go-twitch](https://github.com/knspriggs/go-twitch)

The original work was a bit confusing and more complex than needed (at least in my opinion)

This library uses the newest version of the Twitch API (Current v5)

## Still **Very** Work in Progress

I am adding features as I need them in my other projects.
