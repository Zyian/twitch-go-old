package twitchgo

import (
	"encoding/json"
	"io/ioutil"

	log "github.com/Sirupsen/logrus"
)

//GetStreamByUser gets the user's stream information
func (session *Session) GetStreamByUser(userID string) *Stream {
	tmpData := struct {
		StreamResults *Stream `json:"stream"`
	}{}

	userResp, err := session.sendReq("GET", "/streams/", userID)
	if err != nil {
		log.Error("Error Requesting Stream: ", err)
		return nil
	}

	readBody, err := ioutil.ReadAll(userResp.Body)
	if err != nil {
		log.Error("Error Reading Reponse Body: ", err)
		return nil
	}

	jsonErr := json.Unmarshal(readBody, &tmpData)
	if jsonErr != nil {
		log.Error("Error JSON Unmarshal: ", jsonErr)
		return nil
	}

	userResp.Body.Close()

	return tmpData.StreamResults
}

func (session *Session) GetLiveStreams() {

}

func (session *Session) GetStreamsSummary() {

}

func (session *Session) GetFeaturedStreams() {

}
